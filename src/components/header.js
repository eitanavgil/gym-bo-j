import React from "react";
import { FaCaretLeft } from "react-icons/fa";
import styled from "styled-components";

const Head = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;
const Centered = styled.div`
  padding-top: 0px;
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;

function Header({ title, showBack, onBack }) {
  return (
    <Head onClick={onBack}>
      {showBack && (
        <Centered>
          <Centered>
            <FaCaretLeft size={40} /> <h2>{title}</h2>
          </Centered>
        </Centered>
      )}
    </Head>
  );
}

export default Header;
