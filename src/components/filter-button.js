import React from "react";
import { Button } from "./virtual-keyboard";
import { styled } from "styled-components";

const FilterBut = styled(Button)`
  min-height: 86px;
  min-width: 80px;
  position: relative;
  flex-direction: column;
  padding-left: 4px;
  padding-right: 4px;
  display: flex;
  align-items: flex-start;
  border: none;
`;
const FilterButLarge = styled(Button)`
  border: none;
  min-height: 80px;
  min-width: 130px;
  position: relative;
  flex-direction: column;
  padding-left: 0;
  padding-right: 0;
  display: flex;
  align-items: center;
`;
const Row = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-top: 4px;
  margin-bottom: 2px;
`;
const SmallRow = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-top: 2px;
  margin-bottom: 2px;
  font-size: 12px;
`;

const Third = styled.span`
  font-size: 18px;
  min-width: 32px;
`;
const SmallThird = styled.span`
  font-size: 16px;
  min-width: 14px;
  padding: 0px 3px;
`;
const Notes = styled.span`
  width: 100%;
  font-size: 15px;
  min-height: 20px;
`;
const Name = styled.span`
  width: 100%;
  font-size: 24px;
  padding-top: 0px;
`;
const SmallName = styled.span`
  width: 100%;
  font-size: 18px;
  padding-top: 2px;
  padding-bottom: 2px;
  min-height: 20px;
`;
const Corner = styled.div`
  position: absolute;
  top: 2px;
  right: 6px;
  font-size: 14px;
  font-weight: bold;
`;

function FilterButton({
  displayId,
  displayName,
  onClick,
  sets,
  repititions,
  weight,
  note,
  selected,
  onSelect,
}) {
  return selected ? (
    <>
      <FilterButLarge onClick={onClick}>
        <Name>{displayName || "  "}</Name>
        <Row>
          <Third>S</Third>
          <Third style={{ color: "blue" }}>W</Third>
          <Third style={{ color: "purple" }}>#</Third>
        </Row>
        <Row>
          <Third>{sets || " "}</Third>
          <Third style={{ color: "blue" }}>{weight || " "}</Third>
          <Third style={{ color: "purple" }}>{repititions || " "}</Third>
        </Row>
        <Row>
          <Notes>{note || ""}</Notes>
        </Row>
        <Corner>{displayId}</Corner>
      </FilterButLarge>
    </>
  ) : (
    <>
      <FilterBut onClick={onSelect}>
        <SmallName>{displayName || "  "}</SmallName>
        <SmallRow>
          <SmallThird>S</SmallThird>
          <SmallThird style={{ color: "blue", fontWeight: 700 }}>W</SmallThird>
          <SmallThird style={{ color: "purple", fontWeight: 700 }}>
            #
          </SmallThird>
        </SmallRow>
        <SmallRow>
          <SmallThird>{sets || " "}</SmallThird>
          <SmallThird style={{ color: "blue", fontWeight: 700 }}>
            {weight || " "}
          </SmallThird>
          <SmallThird style={{ color: "purple", fontWeight: 700 }}>
            {repititions || " "}
          </SmallThird>
        </SmallRow>
        <Corner>{displayId}</Corner>
      </FilterBut>
    </>
  );
}

export default FilterButton;
