import React, { useState } from "react";

import { FaPlus, FaRegTimesCircle, FaStopwatch } from "react-icons/fa";
import styled from "styled-components";
import FilterButton from "./filter-button";
import { EditButtonContainer, TransparentButton } from "./gym-device";
import ImageByNumber from "./image-by-number";
import Stopper from "./stopper";
import VirtualKeyboard from "./virtual-keyboard";

export const H1 = styled.h1`
  margin-bottom: 8px;
  margin-top: 12px;
`;
export const H2 = styled.h2`
  margin-bottom: 8px;
  margin-top: 12px;
`;
export const Overlay = styled.div`
  min-width: 100%;
  min-height: 100%;
  background-color: #282c34;
  position: absolute;
  left: 0;
  top: 0;
`;

export const HorizontalDiv = styled.div`
  display: flex;
  justify-content: center;
`;
const ButtonsFlex = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
  flex-direction: row;
  gap: 10px;
`;
export const ImageContainer = styled.div`
  display: flex;
  width: auto;
  justify-content: center;
`;

export const GridContainer = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: repeat(5, 1fr);
  grid-template-rows: repeat(2, 1fr);
  grid-column-gap: 0px;
  grid-row-gap: 0px;
`;

export const musclesGroups = {
  1: "זרוע",
  2: "ארבע ראשי",
  3: "יד אחורית",
  4: "גב",
  5: "חזה",
  6: "בטן",
  7: "זרועות",
  8: "תאומים",
  9: "תחת",
  10: "כתפיים",
};

function FilterGymDevice({ devices, onSelected, onAdd }) {
  const [filteredDevices, setFilteredDevices] = useState([]);
  const [filteredByNumber, setFilteredByNumber] = useState([]);
  const [selectedButton, setSelectedButton] = useState(null);
  const [selectedMuscle, setSelectedMuscle] = useState(null);
  const [onStopper, setOnStopper] = useState(false);
  const filterByArea = (e) => {
    if (selectedMuscle === e) {
      setSelectedMuscle(null);
      setFilteredDevices([]);
      setSelectedButton(null);
      return;
    }
    setSelectedMuscle(e);
    const filtered = devices.filter((d) => d.icon == e);
    setFilteredDevices(filtered);
  };
  // join filteredDevices  filteredByNumber;
  const allFiltered = [...filteredDevices, ...filteredByNumber];
  // uniqe by id
  const uniqe = [
    ...new Map(allFiltered.map((item) => [item.id, item])).values(),
  ];

  return (
    <div
      style={{
        padding: "0 0",
      }}
    >
      <VirtualKeyboard
        onChangedValue={(value) => {
          setFilteredDevices([]);
          setSelectedButton(null);
          setFilteredByNumber(devices.filter((d) => d.deviceNumber === value));
        }}
      />
      <div
        style={{
          marginRight: "36px",
          marginLeft: "36px",
          paddingTop: "16px",
          paddingBottom: "16px",
          maxWidth: "100%",
        }}
      >
        <GridContainer>
          {Object.keys(musclesGroups).map((k) => (
            <ImageContainer
              key={k}
              onClick={() => {
                setSelectedButton(null);
                filterByArea(k);
                setFilteredByNumber([]);
              }}
            >
              <ImageByNumber
                number={k}
                size="percent20"
                selected={selectedMuscle === k}
              />
            </ImageContainer>
          ))}
        </GridContainer>
      </div>
      <div>
        <ButtonsFlex>
          {uniqe.map((d) => (
            <FilterButton
              weight={d.weight}
              repititions={d.repititions}
              sets={d.sets}
              displayId={d.deviceNumber}
              displayName={d.name}
              note={d.notes}
              key={d.id}
              onClick={() => {
                onSelected(d.id);
              }}
              selected={d.id === selectedButton}
              onSelect={() => {
                setSelectedButton(d.id);
              }}
            />
          ))}
        </ButtonsFlex>
      </div>
      <EditButtonContainer>
        <TransparentButton
          onClick={() => {
            setOnStopper(true);
          }}
        >
          <FaStopwatch size={28} />
        </TransparentButton>
        <TransparentButton onClick={onAdd}>
          <FaPlus size={28} />
        </TransparentButton>
      </EditButtonContainer>
      {onStopper && (
        <Overlay>
          <TransparentButton
            style={{ position: "absolute", right: 20, top: 20 }}
            onClick={() => {
              setOnStopper(false);
            }}
          >
            <FaRegTimesCircle size={48} />
          </TransparentButton>
          <Stopper />
        </Overlay>
      )}
    </div>
  );
}

export default FilterGymDevice;
