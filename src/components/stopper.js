import React, { useState } from "react";
import { TransparentButton } from "./gym-device";
import { FaPause, FaPlay, FaRedoAlt } from "react-icons/fa";
import { HorizontalDiv } from "./filter-gym-device";

const Stopper = () => {
  const [isActive, setIsActive] = useState(true);
  const [isPaused, setIsPaused] = useState(false);
  const [time, setTime] = useState(0);

  React.useEffect(() => {
    let interval = null;

    if (isActive && isPaused === false) {
      interval = setInterval(() => {
        setTime((time) => time + 10);
      }, 10);
    } else {
      clearInterval(interval);
    }
    return () => {
      clearInterval(interval);
    };
  }, [isActive, isPaused]);

  const handleStart = () => {
    setIsActive(true);
    setIsPaused(false);
  };

  const handlePauseResume = () => {
    setIsPaused(!isPaused);
  };

  const handleReset = () => {
    setIsActive(false);
    setTime(0);
  };

  return (
    <div style={{ paddingTop: 150 }}>
      <ControlButtons
        active={isActive}
        isPaused={isPaused}
        handleStart={handleStart}
        handlePauseResume={handlePauseResume}
        handleReset={handleReset}
      />
      <Timer time={time} />
    </div>
  );
};

export default Stopper;

export function Timer(props) {
  return (
    <h1 style={{ fontSize: 100 }}>
      <span>{("0" + Math.floor((props.time / 60000) % 60)).slice(-2)}:</span>
      <span>{("0" + Math.floor((props.time / 1000) % 60)).slice(-2)}</span>
    </h1>
  );
}

export function ControlButtons(props) {
  const StartButton = (
    <TransparentButton onClick={props.handleStart}>
      <FaPlay size={48} />
    </TransparentButton>
  );
  const ActiveButtons = (
    <HorizontalDiv>
      <div onClick={props.handlePauseResume}>
        {props.isPaused ? (
          <TransparentButton>
            <FaPlay size={48} />
          </TransparentButton>
        ) : (
          <TransparentButton>
            <FaPause size={48} />
          </TransparentButton>
        )}
      </div>
      <div style={{ minWidth: 40 }}></div>
      <div onClick={props.handleReset}>
        <TransparentButton>
          <FaRedoAlt size={48} />
        </TransparentButton>
      </div>
    </HorizontalDiv>
  );

  return (
    <div>
      <div>{props.active ? ActiveButtons : StartButton}</div>
    </div>
  );
}
