import React from "react";
import I1 from "../mus/mus1.png";
import I2 from "../mus/mus2.png";
import I3 from "../mus/mus3.png";
import I4 from "../mus/mus4.png";
import I5 from "../mus/mus5.png";
import I6 from "../mus/mus6.png";
import I7 from "../mus/mus7.png";
import I8 from "../mus/mus8.png";
import I9 from "../mus/mus9.png";
import I10 from "../mus/mus10.png";

const mapOfImages = {
  1: I1,
  2: I2,
  3: I3,
  4: I4,
  5: I5,
  6: I6,
  7: I7,
  8: I8,
  9: I9,
  10: I10,
};

const sizes = {
  percent20: "60%",
  xs: "50px",
  sm: "85px",
  md: "120px",
  lg: "300px",
};

function ImageByNumber({ number, size = "sm", selected = false }) {
  const width = sizes[size];
  return (
    <img
      src={mapOfImages[number]}
      alt={"muscle-image"}
      style={{
        borderRadius: "12px",
        width,
        background: selected ? "#33EE3333" : "none",
      }}
    />
  );
}

export default ImageByNumber;
