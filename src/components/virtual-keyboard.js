import React from "react";
import { styled } from "styled-components";
import { GridContainer, H1 } from "./filter-gym-device";

export const Button = styled.button`
  margin-top: 8px;
  min-height: 28px;
  min-width: 80%;
  border-radius: 12px;
  font-size: 38px;
  padding: 8px;
  margin-right: 4px;
  margin-left: 4px;
  flex: 0 1 calc(35%);
  border-color: transparent;
`;
function VirtualKeyboard({ onChangedValue }) {
  const [value, setValue] = React.useState();
  const keys = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
  // add one digit to the next value. If the value has already 2 digits remove the first digit and add the new one

  React.useEffect(() => {
    if (value) {
      onChangedValue(value);
    }
  }, [value]);

  const addNumberToExistingValue = (number) => {
    if (value === undefined) {
      setValue(number);
    } else if (value < 10) {
      setValue(10 * value + number);
    } else {
      // take the last digit and add the new one
      setValue((value % 10) * 10 + number);
    }
  };

  return (
    <div style={{ marginRight: "16px", marginLeft: "16px" }}>
      <H1>
        <div
          style={{
            display: "inline-block",
            minWidth: "40px",
            width: "40px",
            textAlign: "left",
          }}
        >
          {value}
        </div>
      </H1>
      <GridContainer>
        {keys.map((k) => {
          return (
            <Button key={k} onClick={() => addNumberToExistingValue(k)}>
              {k}
            </Button>
          );
        })}
      </GridContainer>
    </div>
  );
}

export default VirtualKeyboard;
