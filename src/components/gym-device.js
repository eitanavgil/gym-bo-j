import React from "react";
import ImageByNumber from "./image-by-number";
import { styled } from "styled-components";
import { FaEdit, FaSave, FaUndoAlt, FaCopy, FaTrash } from "react-icons/fa";
import {
  GridContainer,
  ImageContainer,
  musclesGroups,
} from "./filter-gym-device";
import { deleteDataFromLocalStorage } from "../helpers/helpers";
export const TransparentButton = styled.button`
  background: transparent;
  border: none;
  color: white;
  margin-left: 8px;
`;
export const DeleteButton = styled.button`
  background: transparent;
  border: 2px solid;
  color: white;
  margin-left: 8px;
  font-size: 32px;
  padding: 4px 8px;
  border-radius: 8px;
  margin-right: 12px;
`;
const Value = styled.span`
  font-size: 34px;
  font-weight: bold;
  padding-top: 8px;
`;
export const EditButtonContainer = styled.div`
  position: absolute;
  right: 22px;
  top: 22px;
`;
const TextArea = styled.textarea`
  width: 200px;
  height: 100px;
  font-size: 24px;
  padding: 8px;
  margin-top: 12px;
`;
const VerticalDiv = styled.div`
  font-size: 20px;
  min-width: 64px;
  display: inline-flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin: 0 10px;
  padding: 10px;
`;
const Input = styled.input`
  max-width: 50px;
  height: 30px;
  text-align: center;
  font-size: 24px;
  margin-top: 12px;
`;

function GymDevice({ devices, device, onSaved, onCopy }) {
  const { name, icon, weight, repititions, sets, notes, deviceNumber } = device;

  const [newDevice, setNewDevice] = React.useState(deviceNumber === 99);
  const [deleteMode, setDeleteMode] = React.useState(false);
  const [error, setError] = React.useState(null);

  const [values, setValues] = React.useState({
    name,
    icon,
    weight,
    repititions,
    sets,
    notes,
    deviceNumber,
  });
  const [editMode, setEditMode] = React.useState(newDevice);
  if (deleteMode) {
    return (
      <div style={{ padding: "0 20px" }}>
        <h2>? בטוח בטוח</h2>
        <DeleteButton
          onClick={() => deleteDataFromLocalStorage()}
          style={{ color: "#f55" }}
        >
          כן - תמחק
        </DeleteButton>
        <DeleteButton onClick={() => setDeleteMode(false)}>לא</DeleteButton>
      </div>
    );
  }
  return (
    <div>
      <div>
        {!editMode && !newDevice ? (
          <h1>
            {deviceNumber} {deviceNumber && name && " - "} {name}
          </h1>
        ) : (
          <div>
            <Input
              type="number"
              placeholder="מספר"
              value={values.deviceNumber}
              onChange={(e) =>
                setValues({ ...values, deviceNumber: Number(e.target.value) })
              }
              style={{ minWidth: "60px", color: error ? "red" : undefined }}
            />
            <span> - </span>
            <Input
              type="text"
              placeholder="שם מכשיר"
              value={values.name}
              onChange={(e) => setValues({ ...values, name: e.target.value })}
              style={{ minWidth: "140px" }}
            />
          </div>
        )}
        {!editMode ? (
          <EditButtonContainer>
            <TransparentButton
              style={{
                width: "50px",
                height: "58px",
                display: "flex",
                justifyContent: "flex-end",
              }}
              onClick={() => {
                setEditMode(true);
              }}
            >
              <FaEdit size={28} />
            </TransparentButton>
          </EditButtonContainer>
        ) : (
          <EditButtonContainer>
            <TransparentButton onClick={() => setDeleteMode(true)}>
              <FaTrash size={28} color="#f55" />
            </TransparentButton>
            <TransparentButton onClick={onCopy}>
              <FaCopy size={28} color="#999" />
            </TransparentButton>
            <TransparentButton
              onClick={() => {
                if (values.deviceNumber === 99) {
                  setError("מספר מכשיר לא יכול להיות 99");
                  return;
                }
                Object.keys(values).forEach((key) => {
                  device[key] = values[key];
                });
                onSaved();
                setEditMode(false);
              }}
            >
              <FaSave size={28} />
            </TransparentButton>
            <TransparentButton onClick={() => setEditMode(false)}>
              <FaUndoAlt size={28} />
            </TransparentButton>
          </EditButtonContainer>
        )}
        {editMode ? (
          <div style={{ paddingTop: "12px" }}>
            <GridContainer>
              {Object.keys(musclesGroups).map((k) => {
                return (
                  <ImageContainer
                    key={k}
                    onClick={() => setValues({ ...values, icon: k })}
                  >
                    <ImageByNumber
                      number={k}
                      size="xs"
                      selected={k == values.icon}
                    />
                  </ImageContainer>
                );
              })}
            </GridContainer>
          </div>
        ) : (
          <ImageByNumber number={icon} size="md" />
        )}
        <div>
          <VerticalDiv>
            <span>סטים</span>
            {!editMode ? (
              <Value>{sets}</Value>
            ) : (
              <Input
                value={values.sets}
                type="number"
                onChange={(e) =>
                  setValues({ ...values, sets: Number(e.target.value) })
                }
              />
            )}
          </VerticalDiv>
          <VerticalDiv>
            <span>חזרות</span>
            {!editMode ? (
              <Value>{repititions}</Value>
            ) : (
              <Input
                value={values.repititions}
                type="number"
                onChange={(e) =>
                  setValues({ ...values, repititions: Number(e.target.value) })
                }
              />
            )}
          </VerticalDiv>
          <VerticalDiv>
            <span>משקל</span>
            {!editMode ? (
              <Value>{weight}</Value>
            ) : (
              <Input
                value={values.weight}
                type="number"
                onChange={(e) =>
                  setValues({ ...values, weight: Number(e.target.value) })
                }
              />
            )}
          </VerticalDiv>
        </div>
        {(notes || editMode) && (
          <VerticalDiv>
            <>
              <span>הערות</span>
              {!editMode ? (
                <Value style={{ fontSize: "28px", fontWeight: "bolder" }}>
                  {notes}
                </Value>
              ) : (
                <TextArea
                  value={values.notes}
                  onChange={(e) =>
                    setValues({ ...values, notes: e.target.value })
                  }
                />
              )}
            </>
          </VerticalDiv>
        )}
      </div>
    </div>
  );
}

export default GymDevice;
