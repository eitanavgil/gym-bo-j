// set JSON in local storage after stringify
export const setLocalStorage = (key, value) => {
  localStorage.setItem(key, JSON.stringify(value));
};

// get JSON from local storage and parse
export const getLocalStorage = (key) => {
  const value = localStorage.getItem(key);
  return value ? JSON.parse(value) : null;
};

// set data to local storage
export const setDataToLocalStorage = (value) => {
  setLocalStorage("data", value);
};

// get data from local storage
export const getDataFromLocalStorage = () => {
  return getLocalStorage("data");
};

// delete data from local storage
export const deleteDataFromLocalStorage = () => {
  localStorage.removeItem("data");
};

// receive a string and copy it to the clipboard
export const copyToClipboard = (str) => {
  const el = document.createElement("textarea");
  el.value = str;
  document.body.appendChild(el);
  el.select();
  document.execCommand("copy");
  document.body.removeChild(el);
};
