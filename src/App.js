import React from "react";
import "./App.css";

import FilterGymDevice from "./components/filter-gym-device";
import GymDevice from "./components/gym-device";
import Header from "./components/header";
import data from "./data/devices.json";
import {
  copyToClipboard,
  getDataFromLocalStorage,
  setDataToLocalStorage,
} from "./helpers/helpers";

const addNewDevice = (devices) => {
  const newDevice = {
    id: devices.length + 2,
    deviceNumber: 99,
    icon: 3,
    name: "",
    weight: 20,
    repititions: 10,
    sets: 2,
    notes: "",
  };
  return [...devices, newDevice];
};

function App() {
  const savedData = getDataFromLocalStorage();
  const [devices, setDevices] = React.useState(savedData || data);
  const [selectedDevice, setSelectedDevice] = React.useState(null);
  const currentDevice = devices.find((d) => d.id === selectedDevice);
  return (
    <div className="app">
      <div className="app-body">
        {!currentDevice || !selectedDevice ? (
          <FilterGymDevice
            onAdd={() => {
              const newDevices = addNewDevice(devices);
              const justAddedId = newDevices[newDevices.length - 1].id;
              setDevices(newDevices);
              setSelectedDevice(justAddedId);
            }}
            devices={devices}
            onSelected={(s) => {
              setSelectedDevice(s);
            }}
          />
        ) : (
          <>
            <header className="app-header">
              <Header
                onBack={() => setSelectedDevice(null)}
                title={"Back"}
                showBack={!!selectedDevice && !!currentDevice}
              />
            </header>
            <GymDevice
              device={currentDevice}
              devices={devices}
              onCopy={() => {
                copyToClipboard(JSON.stringify(devices));
              }}
              onSaved={() => {
                setSelectedDevice(null);
                setDataToLocalStorage(devices);
                setDevices(data);
                //refresh page
                window.location.reload();
              }}
            />
          </>
        )}
      </div>
    </div>
  );
}

export default App;
